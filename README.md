# oliverAPI: parse outputs and use as features for KPI prediction
The sample code demonstrates how to use oliverAPI outputs for KPI prediction at the "call" (or file) level. 1000 audio files are provided that are labelled as either "emotional" or "neutral"; so the task is a binary emotional vs non-emotional classification task and the associated KPI is "emotional strength" (aka arousal). Oliver API outputs are used as features for a meta-classifier that predicts emotional strength. Optionally one can combine the oliver API features (audio-based) with features extracted from text computed via the open-source vader package. 

The sample code functionality can be broken down into three parts:

 * download and [optionally] preprocess the data, send the data to oliverAPI and get the JSON results back  
 * parse the call-level outputs in the oliverAPI JSON files (and optionally also from the text ouputs provided by vader) in order to select "interesting" files based on filtering criteria, e.g., select files where the "customer" is angry
 * parse the call-level outputs from the oliverAPI JSON files (and optionally also from the text ouputs provided by vader) and use as feautres to train a meta-classifier for the "emotional strenth" binary classification task; provide classification performance results for all features, and stand-alone performance for features (as a simple feature selection first setp), as well as ROCs.   

The basic steps one needs to follow towards this end is:

 1. prepare a dataset, i.e., a set of audio files (either with or without annotation)
 1. send the files to Oliver API
 1. get JSON results from Oliver API
 1. use `oliver_outputs_analysis.py` to retrieve a list of the processed files 
 sorted based on one type, or the sum of all emotional instances
 1. use `oliver_data_use_as_features.py` to demonstrate
how the Oliver API responses can be used as features for solving a call-level (in our case file-level)
(meta)classification task.

### Repo setup ###
pip3 install -r requirements.txt

### Usage example ###
[This emotional speech example dataset (zip, ~300MB)](https://bitbucket.org/behavioralsignals/oliver_data_processing/downloads/emotional_speech_demo.zip)
can be used for evaluating the present code. It's an artificially generated corpus that consists of concatenated segments of emotional or neutral speech. The dataset contains both the audio data and the Oliver API JSON results. 
So here are the basic steps for using the code (You can skip steps 2 and 3 and 
directly use the JSONs found in the zip file):

**Step 1**  
download the sample dataset 
[here](https://bitbucket.org/behavioralsignals/oliver_data_processing/downloads/emotional_speech_demo.zip) (or use your own instead)

**Step 2**  
prepare the data and send them to Oliver API
(use the Command-Line-Interface found 
[here](https://bitbucket.org/behavioralsignals/api-cli/src/master/)  ). Example for stereo audio files:
```
cd emotional_speech_demo
for filename in *.wav; do
    echo $filename,2,1 >> cli.csv
done;

bsi-cli --config bsi_cli.cnf send_audio cli.csv cli.pid
```

**Step 3**  
As soon as the data upload is done you can download the results from the Oliver 
API. We are insterested in getting both audio events and ASR predictions:
```
bsi-cli --config bsi_cli.cnf get_results cli.pid events
bsi-cli --config bsi_cli.cnf get_results_asr cli.pid asr
```
This will download audio jsons in the `events` folder and ASR jsons in the 
`asr` folder. 

*Note 1*: you can skip steps 2 and 3 as this particular dataset contains already 
computed Oliver API results

*Note 2*: you can skip the get_results_asr if you are only interested in using
 audio information alone (no ASR-text-based classifiers). In that case you should 
 use the `-m audio` option (not `text` or `fusion`) described in the next steps.

**Step 4 - Select interesting files**  
Let's proceed with a very basic example of how one could access the outputs of 
the Oliver API and run a simple analysis. 
In particular, one can run `oliver_outputs_analysis.py` for getting all Oliver API
 basic outputs and sort the processed files based on the Oliver API 
 output of interest. For example this command sorts the audio files 
 based on the agents' anger percentage:
```
python3 oliver_outputs_analysis.py -i emotional_speech_demo/events/ -a emotional_speech_demo/asr -m fusion -o agent-angry
```

The result of the above is the following:
```
Filename Value of "agent-angry"
emotional_9_4779112.json 1.0
emotional_58_4779029.json 1.0
emotional_493_4779013.json 1.0
...
emotional_420_4778933.json 0.6
emotional_413_4778925.json 0.6
emotional_408_4778919.json 0.6
emotional_407_4778916.json 0.6
...
neutral_125_4779170.json 0.3
emotional_7_4779042.json 0.3
emotional_73_4779047.json 0.3
...
```

To use all non-neutral behaviors as a sorting measure use `all` in the -o argument (this will sort from the most "emotional" to the least emotional file as the sorting criterion here will be the sum of all non-neutral behaviors) :

```
python3 oliver_outputs_analysis.py -i emotional_speech_demo/events/ -a emotional_speech_demo/asr -m fusion -o all
```

Argument `-m` (modality) can be either audio, text or fusion. 
In case you want to run an experiment without ASR output, you should set this to `audio`.

This is a sample output of the above example. :
```
Filename Value of "all"
emotional_279_4778759.json 5.9559999999999995
emotional_187_4778654.json 5.901999999999999
emotional_233_4778708.json 5.895
emotional_425_4778938.json 5.8919999999999995
emotional_349_4778849.json 5.811
emotional_486_4779005.json 5.777
emotional_68_4779040.json 5.751
emotional_203_4778674.json 5.648
emotional_283_4778764.json 5.642
emotional_441_4778956.json 5.5600000000000
...
emotional_302_4778786.json 3.764
emotional_391_4778898.json 3.758
emotional_450_4778966.json 3.753
emotional_58_4779029.json 3.726
...
emotional_269_4778747.json 1.761
neutral_343_4779417.json 1.746
emotional_81_4779069.json 1.744
emotional_33_4778827.json 1.744
neutral_469_4779560.json 1.734
...
```


Note 1: `oliver_outputs_analysis.py` can also be used with the optional argument 
`--percentage_to_show` (which by default is 100), to limit the returned sorted 
files to the top percentage. E.g. using
`python3 oliver_outputs_analysis.py -i emotional_speech_demo/events/ -a emotional_speech_demo/asr -m fusion -o all -p 1 `
results in the top 1% of the files printed:
```
Filename Value of "all"
emotional_279_4778759.json 5.9559999999999995
emotional_187_4778654.json 5.901999999999999
emotional_233_4778708.json 5.895
emotional_425_4778938.json 5.8919999999999995
emotional_349_4778849.json 5.811
emotional_486_4779005.json 5.777
emotional_68_4779040.json 5.751
emotional_203_4778674.json 5.648
emotional_283_4778764.json 5.642
```

Note 2: `oliver_outputs_analysis.py` can also be used with the optional argument
 `--audio_path`, if the initial audio data is also available. In that case,
 the behavior of the script is identical to the one above, excluding the fact that
 instead of json filenames, the respective audio filenames are printed. E.g.
 `python3 oliver_outputs_analysis.py -i emotional_speech_demo/events/ -a emotional_speech_demo/asr -m fusion -o all -p 1  --audio_path emotional_speech_demo/`
 will return
 ```
emotional_279.wav 5.9559999999999995
emotional_187.wav 5.901999999999999
emotional_233.wav 5.895
emotional_425.wav 5.8919999999999995
emotional_349.wav 5.811
emotional_486.wav 5.777
emotional_68.wav 5.751
emotional_203.wav 5.648
emotional_283.wav 5.642

 ```

**Step 5 - KPI classification and Feature Selection**  
You can now proceed to the evaluation of a meta-classifier that uses the 
Oliver API outputs **as features**. Obviously, to achieve this you will need 
some ground truth (annotation) data, at a file-level. 
For the provided example, this information is in `emotional_speech_demo/annotation.csv`. 
In that case, the ground truth label is 1 if the speech in the respective 
recording is emotional and 0 otherwise. This is an execution example using features from both oliverAPI and vader (fusion):
```
python3 oliver_data_use_as_features.py -i emotional_speech_demo/events/ -g emotional_speech_demo/annotation.csv -a emotional_speech_demo/asr -m fusion
```
This will produce 
(a) a confusion matrix and F1 of the result of a cross-validation 
for the given classification task 
```
F1 = 90.0
Acc = 90.0
[[493   6]
 [ 93 394]]
```

(b) the features sorted by importance (by using them and evaluating them in a simple 1-d classifier) 
```
Feature Name Accuracy achieved
customer-negative 0.7566687280972996
agent-strong 0.7293341579055865
customer-angry 0.724129045557617
agent-negative 0.724025974025974
customer-strong 0.7200061842918987
agent-angry 0.7149969078540508
text-neg 0.6288394145537003
text-pos 0.5700061842918985
agent-happy 0.5558029272314987
agent-weak 0.5355905998763142
customer-happy 0.5122552051123479
customer-positive 0.5061121418264275
agent-positive 0.4888785817357246
customer-weak 0.4878272521129664
customer-sad 0.48475572047000626
agent-sad 0.4807153164296022
```


(c) a ROC and a precision/recall curve

![Precision-Recall and ROC Example](example_roc.png "Precision-Recall and ROC Example")

You can ran this for audio-only 
```
python3 oliver_data_use_as_features.py -i emotional_speech_demo/events/ -g emotional_speech_demo/annotation.csv -a emotional_speech_demo/asr -m audio
F1 = 89.1
Acc = 89.1
[[493   6]
 [101 386]]
```
and text-only 
```
python3 oliver_data_use_as_features.py -i emotional_speech_demo/events/ -g emotional_speech_demo/annotation.csv -a emotional_speech_demo/asr -m text
F1 = 66.7
Acc = 66.7
[[386 113]
 [215 272]]
```

So text-only features performance is at about 67% while for audio-only it is about 89%. You gain an additional 1% in performance by fusion going to 90% classification accuracy.
