import os
import argparse
import numpy as np
import sklearn.metrics
import plotly.subplots
from sklearn.svm import SVC
import plotly.graph_objs as go
from sklearn.metrics import roc_curve
from parsing import read_ground_truth
from sklearn.model_selection import KFold
from oliver_outputs_analysis import get_oliver_outputs_for_folder
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score

mark_prop1 = dict(color='rgba(80, 220, 150, 0.5)',
                  line=dict(color='rgba(80, 220, 150, 1)', width=2))
mark_prop2 = dict(color='rgba(80, 150, 220, 0.5)',
                  line=dict(color='rgba(80, 150, 220, 1)', width=2))
mark_prop3 = dict(color='rgba(250, 150, 150, 0.5)',
                  line=dict(color='rgba(250, 150, 150, 1)', width=3))


def formulate_classication_task(audio_json, asr_json, ground_truth_file, mode):
    # get the oliver outputs for all files in a given folder:
    X, features_names, files = get_oliver_outputs_for_folder(audio_json,
                                                             asr_json, mode)
    # get the respective ground truths
    files_gt, labels = read_ground_truth(ground_truth_file)
    files2 = ["_".join(f.split("_")[0:-1]) for f in files]

    # keep feature vectors only for the raws of X for which we have ground truth
    X_new = []
    files_new = []
    y_new = []
    class_names = []
    for iF, f in enumerate(files_gt):
        if f.replace(".wav", "") in files2:
            X_new.append(X[files2.index(f.replace(".wav", ""))] + 0.00000001)
            files_new.append(f)
            if labels[iF] not in class_names:
                class_names.append(labels[iF])
            y_new.append(class_names.index(labels[iF]))
    return np.array(X_new), np.array(y_new), files_new, features_names, \
           class_names


def svm_train_evaluate(X, y, k_folds, C = 10):
    """
    :param X: Feature matrix
    :param y: Labels matrix
    :param k_folds: Number of folds
    :param C: SVM C param
    :return: confusion matrix, average f1 measure and overall accuracy
    """
    # normalize
    mean, std = X.mean(axis=0), np.std(X, axis=0)
    X = (X - mean) / (std)
    # k-fold evaluation:
    kf = KFold(n_splits=k_folds, shuffle=True)
    f1s, accs, count_cm = [], [], 0
    for train, test in kf.split(X):
        x_train, x_test, y_train, y_test = X[train], X[test], y[train], y[test]
        cl = SVC(kernel='rbf', C=C, gamma="auto")
        cl.fit(x_train, y_train)
        y_pred = cl.predict(x_test)
        # update aggregated confusion matrix:
        if count_cm == 0:
            cm = confusion_matrix(y_pred=y_pred, y_true=y_test)
        else:
            cm += (confusion_matrix(y_pred=y_pred, y_true=y_test))
        count_cm += 1
        f1s.append(f1_score(y_pred=y_pred, y_true=y_test, average='micro'))
        accs.append(accuracy_score(y_pred=y_pred, y_true=y_test))
    f1 = np.mean(f1s)
    acc = np.mean(accs)
    return cm, f1, acc


def evaluate_features(feature_matrix, targets, feature_names):
    """
    Evaluates each feature separately and sorts each feature by
    its ability to discriminate classes for the given classification task

    :param feature_matrix: n_samples x n_features feature matrix
    :param targets:  n_samples x 1 ground truth class labels
    :param feature_names: list of feature names
    :return: prints the features sorted by the most to the least important
             based on its ability to discriminate the classes
    """
    n_features = feature_matrix.shape[1]
    feature_accuracies = []
    for i in range(n_features):
        x_temp = feature_matrix[:, i].reshape(feature_matrix.shape[0], -1)
        kf = KFold(n_splits=5, shuffle=True)
        f1s, accs, count_cm = [], [], 0
        for train, test in kf.split(x_temp):
            x_train, x_test, y_train, y_test = \
                x_temp[train], x_temp[test], targets[train], targets[test]
            cl = SVC(kernel='linear', C=1, gamma="auto")
            cl.fit(x_train, y_train)
            y_pred = cl.predict(x_test)
            # update aggregated confusion matrix:
            if count_cm == 0:
                cm = confusion_matrix(y_pred=y_pred, y_true=y_test)
            else:
                cm += (confusion_matrix(y_pred=y_pred, y_true=y_test))
            count_cm += 1
            f1s.append(f1_score(y_pred=y_pred, y_true=y_test, average='micro'))
        feature_accuracies.append(np.mean(f1s))

    feature_names_s = [x for _, x in sorted(zip(feature_accuracies,
                                                feature_names), reverse=True)]
    feature_accuracies_s = sorted(feature_accuracies, reverse=True)
    print("Feature Name", "Accuracy achieved")
    for iF, f in enumerate(feature_names_s):
        print(feature_names_s[iF], feature_accuracies_s[iF])


def parse_arguments():
    """Parse Arguments for OliverAPI results.
    """
    args_parser = argparse.ArgumentParser(description="Parser for the "
                                                      "API results")
    args_parser.add_argument("-i", "--input", required=True,
                             help="Path of the json files")
    args_parser.add_argument("-g", "--ground_truth", required=True,
                             help="Ground truth file")
    args_parser.add_argument("-a", "--asr", required=True,
                             help="ASR data")
    args_parser.add_argument("-m", "--mode", required=True,
                             choices=["audio", "text", "fusion"],
                             help="Modality used")

    return args_parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    event_folder = args.input
    asr_folder = args.asr
    gt_path = args.ground_truth
    if os.path.isfile(gt_path):
        # if ground-truth file exists then run classification and
        # KPI-level evaluation
        # Step 1: Classifier evaluation through k-fold validation:
        mode = args.mode

        # Step 1A: get oliver features as features and provided csv file
        # (2nd column) as target values
        X, y, f, feature_names, class_names = \
            formulate_classication_task(event_folder,
                                        asr_folder,
                                        gt_path, mode)

        # Step 1B: run SVM k-fold valudation and print F1, Acc
        # and confusion matrix
        cm, f1, acc = svm_train_evaluate(X, y, 10)
        print('F1 = {0:.1f}'.format(100 * f1))
        print('Acc = {0:.1f}'.format(100 * acc))
        print(cm)

        # Step 2: Evaluate the API outputs (and text outputs) as features
        #         for the given KPI classification task
        evaluate_features(X, y, feature_names)

        # Step 3: ROC and Precision - Recall Curves
        # Step 3A: split 50-50 train test
        X_train, y_train = X[::2, :], y[::2]
        X_test, y_test = X[1::2, :], y[1::2]

        # Step 3B: train a simple classifier (with prob output)
        cl = SVC(kernel='linear', C=1, gamma="auto", probability=True)
        cl.fit(X_train, y_train)

        # Step 3C: get posteriors for test data
        probs = cl.predict_proba(X_test)
        probs_positive = [p[1] for p in probs]

        # NOTE: in this example, we are assuming that the class of interest
        # is the 2nd (index 1) class for our ROC and Precision-Recall
        # Step 3D: generate ROC and Precision-Recall curves
        fpr, tpr, thr_roc = roc_curve(y_test, probs_positive)
        pre, rec, thr_pre = \
            sklearn.metrics.precision_recall_curve(y_test,  probs_positive)

        # PLOT results
        figs = plotly.subplots.make_subplots(rows=1, cols=2,
                                             subplot_titles="")
        figs.append_trace(go.Scatter(x=thr_pre, y=pre, name="Precision",
                                     marker=mark_prop1), 1, 1)
        figs.append_trace(go.Scatter(x=thr_pre, y=rec, name="Recall",
                                     marker=mark_prop2), 1, 1)
        figs.append_trace(go.Scatter(x=fpr, y=tpr, showlegend=False), 1, 2)
        figs.update_xaxes(title_text="threshold", row=1, col=1)
        figs.update_xaxes(title_text="false positive rate", row=1, col=2)
        figs.update_yaxes(title_text="true positive rate", row=1, col=2)
        plotly.offline.plot(figs, filename="roc.html", auto_open=True)
