"""!
@brief Script that demonstrates how to read the event JSON files from Oliver API
and aggregate non-neutral content, to list the "most emotional" files

Maintainer: Theodoros Giannakopoulos

@copyright Behavioral Signals Technologies
"""

import os
import argparse
import numpy as np
from parsing import parse_core_outputs, parse_asr_outputs
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


def get_oliver_outputs_for_folder(data_path, asr_path, mode):
    """
    Loops over the JSON files (and ASR files if available) of Oliver API
    responses and parses the basic behavioral outputs and loads them in a matrix

    :param data_path: path of the folder that contains the Oliver audio events
    :param asr_path: path of the folder that contains the ASR data
    :param mode: audio, text or fusion
    :return:
        np.array: [num_of_files x num_of_oliver_api_outputs] matrix
        outputs_names: [num_of_oliver_api_outputs x 1] names of the
        retrieved outputs
        data_files: [num_of_files x 1] filenames
    """
    if os.path.isdir(data_path):
        if os.path.isdir(asr_path):
            asr_files = os.listdir(asr_path)
        else:
            asr_files = []
        data_files = []
        outputs = []
        for iF, f in enumerate(os.listdir(data_path)):
            if f.endswith(".json"):
                asr_file = f.replace(".json", "_words.json")
                if asr_file in asr_files:
                    # read the ASR data and run the text sentiment classifier
                    text = parse_asr_outputs(os.path.join(asr_path, asr_file))
                    analyser = SentimentIntensityAnalyzer()
                    text_result = (analyser.polarity_scores(text))
                    # get the text features
                    fv_text = [text_result["neg"], text_result["pos"]]
                    output_names_text = ["text-neg", "text-pos"]

                data_files.append(f)
                res = parse_core_outputs(os.path.join(data_path, f))
                # Generate the feature vector (and feature names) for audio
                fv_audio = [
                    res["agent"]["emotion"]["angry"],
                    res["agent"]["emotion"]["sad"],
                    res["agent"]["emotion"]["happy"],
                    res["agent"]["positivity"]["negative"],
                    res["agent"]["positivity"]["positive"],
                    res["agent"]["strength"]["strong"],
                    res["agent"]["strength"]["weak"],
                    res["customer"]["emotion"]["angry"],
                    res["customer"]["emotion"]["sad"],
                    res["customer"]["emotion"]["happy"],
                    res["customer"]["positivity"]["negative"],
                    res["customer"]["positivity"]["positive"],
                    res["customer"]["strength"]["strong"],
                    res["customer"]["strength"]["weak"]]
                output_names_audio = \
                    ["agent-angry", "agent-sad", "agent-happy",
                     "agent-negative", "agent-positive",  "agent-strong",
                     "agent-weak",  "customer-angry", "customer-sad",
                     "customer-happy", "customer-negative", "customer-positive",
                     "customer-strong", "customer-weak"]
                if mode == "audio":
                    fv = fv_audio
                    outputs_names = output_names_audio
                elif mode =="text":
                    fv = fv_text
                    outputs_names = output_names_text
                else:
                    fv = fv_audio + fv_text
                    outputs_names = output_names_audio + output_names_text

                outputs.append(fv)
                
        return np.array(outputs), outputs_names, data_files
    else:
        return -1, -1, -1


def parse_arguments():
    """Parse Arguments for OliverAPI results.
    """
    args_parser = argparse.ArgumentParser(description="Parser for the "
                                                      "API results")
    args_parser.add_argument("-i", "--input", required=True,
                             help="Path of the json files")
    args_parser.add_argument("-a", "--asr", required=True,
                             help="ASR data")
    args_parser.add_argument("-m", "--mode", required=True,
                             choices=["audio", "text", "fusion"],
                             help="Modality used")
    args_parser.add_argument("-p", "--percentage_to_show", required=False,
                             default="100",
                             help="Percentage of the top files to show. "
                                  "All files are shown if this is not provided")
    args_parser.add_argument("--audio_path", required=False, 
                             help="Audio path")


    args_parser.add_argument("-o", "--output_to_use_for_sorting",
                             default="all",
                             choices=["agent-angry", "agent-sad", "agent-happy",
                                      "agent-negative", "agent-positive",
                                      "agent-strong", "agent-weak",
                                      "customer-angry", "customer-sad",
                                      "customer-happy", "customer-negative",
                                      "customer-positive",
                                      "customer-strong", "customer-weak",
                                      "text-neg", "text-pos", "all"],
                             help="Which Oliver API output to use for sorting"
                                  "and filtering")
    return args_parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    event_folder = args.input
    asr_folder = args.asr
    modality = args.mode
    output_to_use = args.output_to_use_for_sorting
    percentage_to_show = int(args.percentage_to_show)
    audio_path= args.audio_path

    # parse all Oliver API outputs
    outs, out_names, data_files = get_oliver_outputs_for_folder(event_folder,
                                                                asr_folder,
                                                                modality)
    if output_to_use == "all":
        # Just sum over the Oliver API outputs
        # (percentages for all non-neutral emotions and behaviors)
        # This is a very draft indication of "emotional importance":
        s = outs.sum(axis=1)
    else:
        if output_to_use in out_names:
            output_to_use_index = out_names.index(output_to_use)
            s = outs[:, output_to_use_index]
        else:
            print("Unrecognizable output name! Set to \"all\"")
            s = outs.sum(axis=1)

    if audio_path:
        try:
            tmp = os.listdir(audio_path)
        except FileNotFoundError:
            print("Incorrect audio path. Using json filenames instead.")
        else:
            wav_types = [".wav", ".mp3"]
            wav_types_found = []
            wav_files = []
            for t in tmp:
                if t[-4:] in wav_types:
                    wav_types_found.append(t[-4:])
                    wav_files.append(t)
            new_data_files = []
            for iF, f in enumerate(data_files):
                wav_file_name = ("_".join(f.split("_")[0:-1])) + wav_types_found[iF]
                if wav_file_name in wav_files:
                    new_data_files.append(wav_file_name)
                else:
                    new_data_files.append(f)
            data_files = new_data_files
    files2 = [x for _, x in sorted(zip(s, data_files), reverse=True)]
    s2 = sorted(s, reverse=True)
    n_files_to_show = int(percentage_to_show * len(files2) / 100)
    # ... and print files from most to least "emotional"
    print("Filename", "Value of \"{}\"".format(output_to_use))
    for iF, f in enumerate(files2[0:n_files_to_show]):
        print(files2[iF], s2[iF])
