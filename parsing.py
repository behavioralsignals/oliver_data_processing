import json
import csv


def parse_core_outputs(file_path):
    """
    Parse Oliver API json files (events and behaviors)

    :param file_path: path to json file
    :return: behavioral dictionary of the form:
    {'agent': {'emotion': {'angry': 0.56, 'happy': 0.0, 'sad': 0.0,
               'neutral': 0.44, 'frustrated': 0.0},
               'strength': {'strong': 0.56, 'neutral': 0.44, 'weak': 0.0},
               'positivity': {'negative': 0.22, 'neutral': 0.78,
                             'positive': 0.0}},
    'customer': {'emotion': {'angry': 0.3, 'happy': 0.0, 'sad': 0.0,
                             'neutral': 0.7, 'frustrated': 0.0},
                 'strength': {'strong': 0.9, 'neutral': 0.1, 'weak': 0.0},
                 'positivity': {'negative': 0.9, 'neutral': 0.1,
                                'positive': 0.0}}}
    """
    parsed_core_behaviors = None
    try:
        with open(file_path, "r") as fp:
            results = json.load(fp)
    except IOError:
        print("Could not load file {}".format(file_path))
    except json.decoder.JSONDecodeError:
        print("\n File {} does not follow the event-level API json"
              " format.\n".format(file_path))
        return parsed_core_behaviors

    core_behaviors = results['core']['behaviors']

    parsed_core_behaviors = {"agent": {"emotion": {}, "strength": {},
                                       "positivity": {}},
                             "customer": {"emotion": {}, "strength": {},
                                          "positivity": {}}}
    for c in core_behaviors:
        task = c['key']
        for labels in c['value']:
            if task in parsed_core_behaviors["customer"]:
                parsed_core_behaviors["customer"][task][labels["label"]] = \
                    labels["value"]["customer"]
            if task in parsed_core_behaviors["agent"]:
                parsed_core_behaviors["agent"][task][labels["label"]] =\
                    labels["value"]["agent"]

    return parsed_core_behaviors


def parse_asr_outputs(file_path):
    """
    Parse Oliver API ASR files

    :param file_path: path to the ASR file
    :return: ASR results (string of all the recognized words concatenated)
    """
    parsed_asr = None
    try:
        with open(file_path, "r") as fp:
            results = json.load(fp)
    except IOError:
        print("Could not load file {}".format(file_path))
    except json.decoder.JSONDecodeError:
        print("\n File {} does not follow the event-level API json"
              " format.\n".format(file_path))
        return parsed_asr

    parsed_asr = " ".join([w['w'] for w in results['words']])
    return parsed_asr


def read_ground_truth(filename):
    """
    Read ground truth file for call-level KPI classifier
    training and/or evaluation

    :param filename: CSV file with 2 columns: <file path>, <KPI class label>
    :return:
    """
    with open(filename, 'rt') as f_handle:
        reader = csv.reader(f_handle, delimiter=',')
        files = []
        labels = []
        for ir, row in enumerate(reader):
            if ir > 0:
                labels.append(int(row[1]))
                files.append(row[0])
    return files, labels
